const express = require('express');
const bodyParser = require('body-parser');
const Sequelize = require('sequelize');
const cors = require('cors');
const uuidv1 = require('uuid/v1');
const Crypto = require('crypto');

const app = express();
app.use(bodyParser.json());
app.use(cors());

//SERVER AUTH

const sequelize = new Sequelize('c9', 'eduardgheorghiu', '', {
   host: 'localhost',
   dialect: 'mysql',
   operatorsAliases: false,
   pool: {
        "max": 1,
        "min": 0,
        "idle": 20000,
        "acquire": 20000
    }
});

sequelize
  .authenticate()
  .then(() => {
    console.log('Connection has been established successfully.');
  })
  .catch(err => {
    console.error('Unable to connect to the database:', err);
  });

//Define the User Model

const User = sequelize.define('users', {
    id: {
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
    },
   name: {
       type: Sequelize.STRING,
       allowNull: false
   }, 
   surname: {
        type: Sequelize.STRING,
        allowNull: false
   },
   username: {
       type: Sequelize.STRING,
       allowNull: false,
       unique: true
   }, 
   password : {
       type: Sequelize.STRING,
       allowNull: false
   },
   type : {
       type : Sequelize.STRING,
       allowNull: false
   }
});

const Session = sequelize.define('sesions',
{
    id :{ 
        type:Sequelize.INTEGER,
        primaryKey: true,
        allowNull : false,
        autoIncrement: true
    },

    token :{
        type: Sequelize.STRING,
        allowNull : false
    }
});

const Test= sequelize.define('test',
{
    id :{
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
    },
    name : {
        type: Sequelize.STRING,
        allowNull: false,
    },
    partial : {
        type: Sequelize.BOOLEAN,
        allowNull : false,
    },
    time : {
        type: Sequelize.INTEGER,
        allowNull : false
    },
    topic : {
        type : Sequelize.STRING,
        allowNull: false
    }
});

const TestInstance = sequelize.define('testinstances',
{
    id:{
        type: Sequelize.STRING,
        primaryKey : true,
        allowNull : false
    },
    time : {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    partial : {
        type: Sequelize.BOOLEAN,
        allowNull : false
    },
    numberOfParticipants : {
        type : Sequelize.INTEGER,
        allowNull : false
    },
    token : {
        type : Sequelize.STRING
    },
    isActive : {
        type : Sequelize.BOOLEAN,
        allowNull : false
    }
})

const Question = sequelize.define('questions',
{
    id :{
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
    },
    questionField1 :{
        type: Sequelize.STRING,
        allowNull : false
    },
    questionField2 : {
        type: Sequelize.STRING
    }
})

const Answer = sequelize.define('answers',{
    id :{
        type: Sequelize.STRING,
        primaryKey: true,
        allowNull: false
    },
    answerField : {
        type: Sequelize.STRING,
        allowNull : false
    },
    isCorect : {
        type : Sequelize.BOOLEAN,
        allowNull : false
    }
});

const PTLink = sequelize.define('ptlinks', {
    id : {
        type: Sequelize.INTEGER,
        autoIncrement : true,
        primaryKey : true,
        allowNull : false
    }
})

const TQLink = sequelize.define('tqlinks',{
    id :{
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    }
})

const InstanceGrade = sequelize.define('instancegrades',{
    id : {
        type: Sequelize.INTEGER,
        autoIncrement: true,
        primaryKey: true,
        allowNull: false
    },
    grade : {
        type : Sequelize.FLOAT,
        allowNull : false
    }
})

User.hasMany(InstanceGrade, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });
TestInstance.hasMany(InstanceGrade, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });

User.hasMany(Test, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });

User.hasMany(TestInstance, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });
Test.hasMany(TestInstance, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });

User.hasMany(PTLink, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });
Test.hasMany(PTLink, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });

Test.hasMany(TQLink, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });
Question.hasMany(TQLink, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });

Question.hasMany(Answer, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });

User.hasOne(Session, { foreignKey: { allowNull: false }, onDelete: 'CASCADE' });

sequelize.sync({
    //force: true
});
//SERVER
app.listen(8080, ()=>{
    console.log('Server started on port 8080...');
})

app.use(function(req, res, next){ 
    res.header("Access-Control-Allow-Origin", "*"); 
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next(); 
}); 


app.post('/register', (req, res) =>{
    let guid = uuidv1();
    User.create({
        id: guid,
        name: req.body.name,
        surname: req.body.surname,
        username: req.body.username,
        password: req.body.password,
        type: 'student'
    }).then((user) => {
        res.status(201).send("User created successfully");
    }, (err) => {
        res.status(500).send(err);
    });
    Session.create({
        userId: guid,
        token : "empty"
    }).then((session) => {
        res.status(201).send("Session created successfully");
    }, (err) => {
        res.status(500).send(err);
    })
})

app.put('/users' , (req,res) => {
    User.update({
        name : req.body.name,
        surname : req.body.surname,
        username : req.body.username,
        password : req.body.password
    },
    {returning : true,
    where: {id : req.body.id}})
    .then(function(){
        res.status(202).send("User updated");
    })
})

app.get('/log/login', (req,res) =>
{
    
    User.findOne({where: {username: req.query.username, password: req.query.password}} ).then((result) => {
       if(result){
           res.status(200).send(result)
       }
        else res.status(504).send('User not found')
})
});



app.post('/login', (req, res) => {
   User.findOne({where:{username: req.body.username, password: req.body.password} }).then(  function (result) {
       if(result){
           Crypto.randomBytes(48, function(err, buffer) {
            var t = buffer.toString('hex');
            var tt =t.substring(0,24);
            Session.update({
                token: tt},
                {where: {userId : result.dataValues.id}});
           res.status(200).send(result)
                 });

           
       }
        else res.status(504).send('User not found')

   }) 

});


app.get('/log', (req,res) => {
    User.findAll().then((users) =>{
        res.status(200).send(users);    
    });
})


app.get('/token', (req, res) => {
      User.findOne({where: {username: req.query.username, password: req.query.password}} ).then((result) => {
       if(result){
           
           var token = result.dataValues.id;
           Session.findOne({where :{userId : token}}).then((newResult) => {
               if(newResult)
                res.status(200).send(newResult);
               else
                res.status(504).send('Session not found')
                    
           });
       }
        else res.status(504).send('User not found')
})

})

app.get('/userId', (req,res) => {
    Session.findOne({where : {token : req.query.token}}).then((result) => {
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('Token not found');
        }
    })
});

app.get('/log/guid', (req,res) => {
    User.findOne({where: {id : req.query.id}}).then((result) =>{
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No user found');
        }
    })
})

app.get('/test', (req,res) => {
    Test.findOne({where : { id : req.query.id}}).then((result) =>{
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No test found');
        }
    })
})

app.get('/testName', (req,res) => {
    Test.findOne({where : { name : req.query.name}}).then((result) =>{
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No test found');
        }
    })
})

app.get('/teacherTest', (req,res) => {
    Test.findAll({where : { userId : req.query.userId}}).then((result) =>{
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No test found');
        }
    })
})


app.get('/question', (req,res) => {
    Question.findOne({where : { id : req.query.id}}).then((result) =>{
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No question found');
        }
    })
})

app.get('/allQuestions', (req,res) => {
    Question.findAll().then((result) => {
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No questions')
        }
    })
})


app.get('/tqlink', (req,res) => {
    TQLink.findOne({where : { id : req.query.id}}).then((result) =>{
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No link found');
        }
    })
})

app.get('/answer', (req,res) => {
    Answer.findOne({where : { id : req.query.id}}).then((result) =>{
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No answer found');
        }
    })
})

app.get('/answer/question', (req,res) => {
    Answer.findAll({where : { questionId : req.query.questionId}}).then((result) =>{
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No answers found');
        }
    })
})

app.get('/test/question' , (req,res) => {
    Test.findAll({include: [
     { model : TQLink, where: { testId: req.query.id }, required: true}]},
     {where : { id : req.query.id } }
  ).then((result) =>{
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No questions found');
        }
    })
})





app.get('/question/test', (req,res) => {
    TQLink.findAll({where : { testId : req.query.testId}}).then((result) =>{
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No question found');
        }
    })
})


app.post('/test', (req, res) =>{
    let guid = uuidv1();
    Test.create({
        id: guid,
        name: req.body.name,
        partial: req.body.partial,
        time: req.body.time,
        userId : req.body.userId,
        topic : req.body.topic
    });
    PTLink.create({
        userId : req.body.userId,
        testId : guid
    });
    res.status(200).send(`${guid} Test create successfully`);
})


app.delete('/test', (req,res) => {
    Test.destroy({
        where: {
      id: req.body.id
    }
    }).then((result) => {
        res.status(200).send('Test deleted successfully')
    }).catch((err) => {
        res.status(500).send('No test found');
        console.log(err);
    })
})

app.post('/question',(req,res) => {
    var guid =uuidv1();
    if(req.body.question.length >= 256)
    {
     var q1 = req.body.question.substring(0,255);
     var q2 = req.body.question.substring(255,req.body.question.length);
    }
    else{
        var q1= req.body.question;
        var q2= "";
    }
    Question.create({
        id : guid,
        questionField1 : q1,
        questionField2 : q2
    });
    TQLink.create({
        testId : req.body.testId,
        questionId : guid
    });
    var numberOfAnswers = req.body.numberOfAnswers;
    var answersArray = req.body.answers;
    console.log(answersArray);
    for(let i = 0 ; i < numberOfAnswers ; i++)
    {
        let newGuid = uuidv1()
        Answer.create({
            id : newGuid,
            answerField : answersArray.answer[i],
            isCorect : answersArray.validity[i],
            questionId : guid
        })
    }
    res.status(200).send('Question created successfully');
    
})

app.get('/testinstance', (req,res) => {
    TestInstance.findAll({where : {userId : req.query.userId, testId : req.query.testId}}).then((result) =>{
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No testinstance found');
        }
    })
})

app.get('/testinstancespecific', (req,res) => {
    TestInstance.findOne({where : {userId : req.query.userId, testId : req.query.userId, id : req.query.id}}).then((result) =>{
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No testinstance found');
        }
    })
})

app.get('/alltestinstance', (req,res) => {
    TestInstance.findAll().then((result) => {
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No testinstances found');
        }
    })
})

app.post('/addtestinstance', (req,res) => {
    var guid = uuidv1();
    TestInstance.create({
        id : guid,
        time : req.body.time,
        partial : req.body.partial,
        numberOfParticipants : 0,
        userId : req.body.userId,
        testId : req.body.testId,
        isActive : false,
        token : Math.random().toString(36).substring(2,8)
        }).then((test) => {
        res.status(201).send(test);
    }, (err) => {
        res.status(500).send(err);
    });
})

app.put('/testinstanceID', (req,res) => {
    TestInstance.update({
        time : req.body.time,
        partial : req.body.partial,
        numberOfParticipants : req.body.numberOfParticipants,
        isActive : req.body.isActive
    },
    {returning : true,
    where: {id : req.body.id}})
    .then(function(instance){
        var obj = `{ \"status\" : \"Instance update successfully\"} `
        res.status(202).send(obj);
    })
})

app.post('/instancegrade', (req,res) => {
    InstanceGrade.create({
        grade: req.body.grade,
        userId : req.body.userId,
        testinstanceId : req.body.testinstanceId
    }).then((result) => {
        res.status(200).send(result)
    }), (err) => {
        res.status(500).send(err);
    }
})

app.get('/instancegrades', (req,res) => {
    TestInstance.findAll({where : {testId : req.query.testId}}).then(async (result) => {
        if(result)
        {
            var grades =[]
            for(let i = 0 ; i <result.length ; i++)
                await InstanceGrade.findAll(({where : {testinstanceId : result[i].id}}))
                .then((result2) => {
                    if(result2.length)
                        grades.push(result2);
                       
                })
                res.status(200).send(grades);
        }
        else
        {
            res.status(500).send('No test found')
        }
        
    })
})

app.get('/allinstancegrades' , (req,res) => {
    InstanceGrade.findAll().then((result) =>{
        if(result){
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send("No instance grades")
        }
    });
})

app.get('/instancegradesstudent', (req,res) => {
    InstanceGrade.findAll({ where : {userId : req.query.userId}}).then((result) => {
        if(result)
        {
            res.status(200).send(result);
        }
        else
        {
            res.status(500).send('No tests for this user');
        }
    })
})

app.get('/' , (req,res) => {
    res.status(200).send('Main');
});

