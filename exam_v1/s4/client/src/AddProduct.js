import React from 'react';
import axios from 'axios';

export class AddProduct extends React.Component{
    constructor(props){
        super(props);
        this.state={
            productName: "",
            price:0
    };
}

clearFields=() =>{
    this.setState({
        productName:" ",
        price:0
    });
}

handleChangeProductName=(e)=>{
    this.setState({
        productName:e.target.value
    });
}

handleChangePrice=(e)=>{
    this.setState({
        price:e.target.value
    });
}

onProductAdded=()=>{
    let product={
        productName: this.state.productName,
        price: this.state.price
    }
    
    axios.post('https://webtech-eduardgheorghiu.c9users.io:8080/add', {
      productName: this.state.productName,
      price: this.state.price
    })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });
    
    this.props.handleAdd(product);
    
    this.clearFields();
}
    
   render(){
       return(

           <div>
           <h1>Add Product</h1>
            <input type="text" placeholder="Product Name" value={this.state.productName} onChange={this.handleChangeProductName}/>
            <input type="number" value={this.state.price}  onChange={this.handleChangePrice}/>
            <button onClick={this.onProductAdded}>Add Product</button>
            </div>

       )
   } 
}