import React, { Component } from 'react';
import './App.css';
import axios from 'axios';
import {AddProduct} from'./AddProduct.js'
import {ProductList} from './ProductList.js'

class App extends Component {
  constructor(props){
    super(props)
    this.state={}
    this.state.products=[]
  }
  
  componentDidMount(){
   
    this.getList();
    
  }
  
  getList = () => {
    var place = this;
    axios.get('https://webtech-eduardgheorghiu.c9users.io:8080/get-all', {
   })
  .then(function (response) {
    
    if(response.status === 200)
      place.setState({
        products : response.data
      })
  })
  .catch(function (error) {
    console.log(error);
  })
  };
  
  
  onAdd=(product)=>{
    this.state.products.push(product);
    let products=this.state.products;
    this.setState({
      products:products
    })
  }
  render() {
    return (
     <React.Fragment>
     <div>
      <AddProduct handleAdd={this.onAdd}/>
      <ProductList title="Products" source={this.state.products}/>
      </div>
      </React.Fragment>
    );
  }
}

export default App;
